from tkinter.constants import TRUE
from typing import Text
import pandas as pd
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from clases.programa import programa
from tkcalendar import Calendar
from tkinter import messagebox

class filtrar_info_2:
        def __init__(self, db, padre):
            self.padre = padre
            self.db = db
            self.filtrar()

        def filtrar(self):
            self.filtrar = tk.Toplevel()
            self.filtrar.geometry("500x250")
            self.filtrar.resizable(width=0, height=0)
            tk.Label(self.filtrar, text =  "Seleccione sexo para filtrar").place(x = 30, y = 10, width = 450, height = 20)                
            tk.Label(self.filtrar, text =  "Sexo: ").place(x = 30, y = 100, width = 200, height = 20)
            sql = "select * from sexo"
            self.data_sexo = self.db.run_select(sql)

            lista_sexo = pd.DataFrame(self.data_sexo)
            self.combo_sexo = ttk.Combobox(self.filtrar, width=20, state = "readonly")
            self.combo_sexo.place(x = 170, y = 100)
            self.combo_sexo["values"] = lista_sexo[1].tolist()
            self.combo_sexo.bind("<<ComboboxSelected>>", self.prueba)
            tk.Button(self.filtrar, text="Filtrar",
                    command = self.procedimiento).place(x = 200, y = 150, width = 100, height = 50)

  
        def prueba(self, event):
            for i in self.data_sexo:
                if self.combo_sexo.get() in i:
                    self.sexo_fin = i[0]
                else:
                    pass
            
        def procedimiento(self):
            sexo = str(self.sexo_fin)
            if self.sexo_fin == 0:
                self.warning_2()
            else:
                text = "call filtrar_paciente ("+sexo+")"
                sql = text
                hola = self.db.run_select(sql)
                self.mostrar(hola)
                
        def mostrar(self, data):
            self.mostrar_data = tk.Toplevel()
            self.mostrar_data.geometry('600x300')
            self.mostrar_data.title("Pacientes")
            self.mostrar_data.resizable(width=0, height=0)
            self.treeview_filtrar_1(data)
                
        def treeview_filtrar_1(self, data):
            self.treeview_filtrar = ttk.Treeview(self.mostrar_data)
            self.treeview_filtrar.configure(columns = ("#1", "#2"))
            self.treeview_filtrar.heading("#0", text = "Rut")
            self.treeview_filtrar.heading("#1", text = "Nombre Paciente")
            self.treeview_filtrar.heading("#2", text = "Apellido Paciente")
            self.treeview_filtrar.column("#0", minwidth = 275, width = 200, stretch = False)        
            self.treeview_filtrar.column("#1", minwidth = 275, width = 200, stretch = False)
            self.treeview_filtrar.column("#2", minwidth = 275, width = 200, stretch = False)
            self.treeview_filtrar.place(x = 0, y = 0, height = 350, width = 700)
            self.llenar_dato(data)

        def llenar_dato(self, data):
            print(data, "hola")
            daton = []
            if(data != daton):
                # Elimina todos los rows del treeview
                self.treeview_filtrar.delete(*self.treeview_filtrar.get_children())
                for i in data:
                    print(i)
                    #Inserta los datos
                    self.treeview_filtrar.insert("", "end", text = i[0], values = (i[1], i[2]), iid = i[0])
                daton = data
        def warning_2(self):
            messagebox.showinfo("Advertencia", "Por favor ingrese un valor")