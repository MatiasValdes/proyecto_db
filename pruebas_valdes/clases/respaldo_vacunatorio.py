import tkinter as tk
from tkinter import ttk
from clases.inventario import inventario
from tkinter import messagebox as MessageBox
import pandas as pd

class vacunatorio_respaldo:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Respaldo de vacunatorios")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_vacunatorio()
        self.config_buttons_vacunatorio()
 
    def config_treeview_vacunatorio(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Aforo")
        self.treeview.heading("#3", text = "Ciudad")
        self.treeview.column("#0", minwidth = 50, width = 50, stretch = False)
        self.treeview.column("#1", minwidth = 372, width = 200, stretch = False)
        self.treeview.column("#2", minwidth = 372, width = 200, stretch = False)
        self.treeview.column("#3", minwidth = 372, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 600)
        self.llenar_treeview_vacunatorio()

    def config_buttons_vacunatorio(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Aceptar", 
                  command = self.root.destroy).place(x = 0, y = 350, width = 300, height = 50)
        tk.Button(self.root, text="Cancelar",
                  command = self.root.destroy).place(x = 300, y = 350, width = 300, height = 50)

    def llenar_treeview_vacunatorio(self):#Se llena el treeview de datos. 
        #sql = """select vacunatorio.id_vacunatorio, vacunatorio.nombre_vacunatorio, 
        #    vacunatorio.aforo, ciudad.nombre_ciudad from vacunatorio 
        #    inner join ciudad on vacunatorio.ciudad_id_ciudad = ciudad.id_ciudad"""
        sql = "select * from respaldo_vacunatorio"
        # Ejecuta el select
        data = self.db.run_select(sql)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2],i[3]), iid = i[0])
            self.data = data#Actualiza la data"""