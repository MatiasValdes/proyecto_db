import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as MessageBox

class trabajador:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('500x500')
        self.root.title("Trabajadores")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        
        self.config_treeview_trabajador()
        self.config_buttons_trabajador()
 
    def config_treeview_trabajador(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Apellido")
        #self.treeview.heading("#3", text = "Vacunatorio")
        self.treeview.column("#0", minwidth = 50, width = 51, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 200, stretch = False)
        self.treeview.column("#2", minwidth = 275, width = 250, stretch = False)
        # self.treeview.column("#3", minwidth = 275, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 879)
        self.llenar_treeview_trabajador()


    def config_buttons_trabajador(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar Trabajador", 
                  command = self.llamar).place(x = 0, y = 450, width = 150, height = 50)
        tk.Button(self.root, text="Modificar Trabajador",
                  command = self.modif).place(x = 150, y = 450, width = 150, height = 50)
        tk.Button(self.root, text="Eliminar Trabajador",
                  command = self.elimin).place(x = 300, y = 450, width = 200, height = 50)

    def llenar_treeview_trabajador(self):#Se llena el treeview de datos. 
        #sql = """select trabajador.id_trabajador, trabajador.nombre_trabajador, trabajador.apellido_trabajador, vacunatorio.nombre_vacunatorio from trabajador 
        #inner join programa on programa.trabajador_id_trabajador = trabajador.id_trabajador 
        #inner join vacunatorio on programa.vacunatorio_id_vacunatorio = vacunatorio.id_vacunatorio"""
        sql = "select * from trabajador"
        print(sql)
        # Ejecuta el select
        data = self.db.run_select(sql)
        print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2]), iid = i[0])
            self.data = data#Actualiza la data"""



    def llamar(self):
        print("insertar")
        self.insertar_trabajador(self.db, self)

    def modif(self):
        print("modificar")
        sql = "select * from trabajador where id_trabajador = %(id_trabajador)s"
        row_data = self.db.run_select_filter(sql, {"id_trabajador": self.treeview.focus()})
        self.modificar_trabajador(self.db, self, row_data)

    def elimin(self):
        sql = "select * from trabajador where id_trabajador = %(id_trabajador)s"
        lista = self.db.run_select_filter(sql, {"id_trabajador": self.treeview.focus()})
        self.warning(lista)
    
    def warning(self, lista):
        for i in lista:
            lista = list(i)
        
        id = lista[0]
        nombre = lista[1]
        apellido = lista[2]
        # Se obtiene el nombre elegido
        print(nombre)
        
        text = "¿Seguro que desea eliminar al trabajador " + nombre + apellido + "?"
        resultado = MessageBox.askquestion("Eliminar", text)
        # Si desea eliminarlo, se ejecuta la opción en mysql
        if resultado == "yes":
            self.elim(id)
        else:
            pass

    # Función de eliminación por orden de mysql
    def elim(self, id):
        # Eliminación en la base de datos.
        sql = """delete trabajador from trabajador where id_trabajador = %(id_trabajador)s"""
        # Especificamos el nombre que se quiere eliminar
        self.db.run_sql(sql, {"id_trabajador": id})
        self.llenar_treeview_ciudad()
    

    class insertar_trabajador:
        def __init__(self, db, padre):
            self.padre = padre
            self.db = db
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Settings
            self.insert_datos.geometry('400x190')
            self.insert_datos.title("Insertar Vacunatorio")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Labels
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Apellido: ").place(x = 10, y = 40, width = 130, height = 20)
    
        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_apellido = tk.Entry(self.insert_datos)
            self.entry_apellido.place(x = 170, y = 40, width = 150, height = 20)
    
        def config_button(self):#Se configura el boton
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.insertar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar", 
                command = self.insert_datos.destroy).place(x=200, y =170, width = 200, height = 20)

        def insertar(self): #Insercion en la base de datos. 
            sql = "insert into trabajador (nombre_trabajador, apellido_trabajador) values (%(nombre_trabajador)s, %(apellido_trabajador)s)"
            self.db.run_sql(sql, {"nombre_trabajador": self.entry_nombre.get(), 
                                  "apellido_trabajador": self.entry_apellido.get()})
            self.insert_datos.destroy()
            self.llenar_treeview_trabajador()


    class modificar_trabajador:#Clase para modificar
        def __init__(self, db, padre, row_data):
            self.padre = padre
            self.db = db
            self.row_data = row_data
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Configuración de la ventana. 
            self.insert_datos.geometry('400x190')
            self.insert_datos.title("Modificar Vacunatorio")
            self.insert_datos.resizable(width=0, height=0)
    
        def config_label(self):#Se configuran las etiquetes.
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Apellido: ").place(x = 10, y = 40, width = 130, height = 20)
    
        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_apellido = tk.Entry(self.insert_datos)
            self.entry_apellido.place(x = 170, y = 40, width = 150, height = 20)

            print(self.row_data)
            self.entry_nombre.insert(0, self.row_data[0][1])
            self.entry_apellido.insert(0, self.row_data[0][2])

        def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado. 
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.modificar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar",
                    command = self.insert_datos.destroy).place(x=200, y=170, width=200, height=20)

        def modificar(self): #Insercion en la base de datos. 
            sql = "update trabajador set nombre_trabajador = %(nombre_trabajador)s, apellido_trabajador = %(apellido_trabajador)s where id_trabajador = %(id_trabajador)s"

            self.db.run_sql(sql, {"nombre_trabajador": self.entry_nombre.get(), 
                                "apellido_trabajador": self.entry_apellido.get(),
                                "id_trabajador" : int(self.row_data[0][0])})

            self.insert_datos.destroy()
            self.padre.llenar_treeview_trabajador()
