from tkinter.constants import TRUE
from typing import Text
import pandas as pd
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from clases.programa import programa
from tkcalendar import Calendar
from tkinter import messagebox
from clases.filtrar_paciente import filtrar_info_2

class paciente:
    def __init__(self, root, db):
        self.db = db
        self.data = []
        
        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('800x600')
        self.root.title("Pacientes")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_paciente()
        self.config_buttons_paciente()

    def config_treeview_paciente(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3", "#4", "#5"))
        self.treeview.heading("#0", text = "RUT")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Apellido")
        self.treeview.heading("#3", text = "Fecha Nacimiento")
        self.treeview.heading("#4", text = "Sexo")
        self.treeview.heading("#5", text = "Ciudad")
        self.treeview.column("#0", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#2", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 275, width = 250, stretch = False)
        self.treeview.column("#4", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#5", minwidth = 275, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 1650)
        self.llenar_treeview_paciente()

    def config_buttons_paciente(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar", 
                  command = self.llamar).place(x = 0, y = 550, width = 200, height = 50)
        tk.Button(self.root, text="Modificar",
                  command = self.modif).place(x = 200, y = 550, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar",
                  command = self.elim).place(x = 400, y = 550, width = 100, height = 50)
        tk.Button(self.root, text="Programa Paciente",
                  command = self.program, foreground="red", background="#d7f7ff").place(x = 500, y = 550, width = 200, height = 50)
        tk.Button(self.root, text="Filtrar",
                  command = self.filtrar).place(x = 700, y = 550, width = 100, height = 50)

    def llenar_treeview_paciente(self):#Se llena el treeview de datos. 
        #sql = ("select paciente.rut, paciente.nombre_paciente, paciente.apellido_paciente, paciente.fecha_nacimiento,"+
        #        "sexo.nombre_sexo, ciudad.nombre_ciudad from paciente inner join sexo on sexo.id_sexo = paciente.sexo_id_sexo "+
        #        "inner join ciudad on ciudad.id_ciudad = paciente.ciudad_id_ciudad")
        sql = "select * from view_paciente"
        # Ejecuta el select
        data = self.db.run_select(sql)
        #print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2],i[3],i[4],i[5]), iid = i[0])
            self.data = data#Actualiza la data"""

    def filtrar(self):
        filtrar_info_2(self.db, self)

    def llamar(self):
        print("insertar")
        self.insertar_paciente(self.db, self)

    def modif(self):
        print("modificar")
        sql = "select * from paciente where rut = %(rut)s"
        row_data = self.db.run_select_filter(sql, {"rut": self.treeview.focus()})
        modificar_paciente(self.db, self, row_data)

    def elim(self):
        # print("eliminar")
        sql = "select * from paciente where rut = %(rut)s"
        lista = self.db.run_select_filter(sql, {"rut": self.treeview.focus()})
        self.warning(lista)


    def warning(self, lista):
        for i in lista:
            lista = list(i)
        
        rut = lista[0]
        nombre = lista[1]
        apellido = lista[2]

        text = "¿Seguro que desea eliminar el siguiente paciente? \nRut: " + rut + " \nNombre: " + nombre + "\nApellido: " + apellido
        resultado = messagebox.askquestion("Eliminar", text)
        # Si desea eliminarlo, se ejecuta la opción en mysql
        if resultado == "yes":
            self.elimin(rut)
        else:
            pass

    # Función de eliminación por orden de mysql
    def elimin(self, rut):
        # Eliminación en la base de datos.
        sql = """delete paciente from paciente where rut = %(rut)s"""
        # Especificamos el nombre que se quiere eliminar
        self.db.run_sql(sql, {"rut": rut})
        self.llenar_treeview_paciente() 
    
    
    def program(self):
        print("programa paciente")
        sql = "select * from paciente where rut = %(rut)s"
        row_data = self.db.run_select_filter(sql, {"rut": self.treeview.focus()})

        mostrar = row_data[0][0]
        print(mostrar)
        programa(self.root, self.db, mostrar)

    

    class insertar_paciente:
        def __init__(self, db, padre):
            self.padre = padre
            self.db = db
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Settings
            self.insert_datos.geometry('500x500')
            self.insert_datos.title("Insertar Paciente")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Labels
            tk.Label(self.insert_datos, text =  "RUT: ").place(x = 10, y = 200, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 250, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Apellido: ").place(x = 10, y = 300, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Fecha Nacimiento:").place(x = 5, y = 15, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Sexo: ").place(x = 10, y = 350, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Ciudad: ").place(x = 10, y = 400, width = 130, height = 20)

        def config_entry(self):#Se configuran los inputs

            self.entry_rut = tk.Entry(self.insert_datos)
            self.entry_rut.place(x = 170, y = 200, width = 150, height = 30)
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 250, width = 150, height = 30)
            self.entry_apellido = tk.Entry(self.insert_datos)
            self.entry_apellido.place(x = 170, y = 300, width = 150, height = 30)
            
            self.cal = Calendar(self.insert_datos, selectmode="day", year = 2021, month=2, day=5)#.place(x=170, y=300)
            self.cal.pack(pady=20)
            # cal.pack(fill="both", expand=True)
            #self.entry_fecha = tk.Entry(self.insert_datos)
            #self.entry_fecha.place(x = 170, y = 200, width = 150, height = 30)

            sql = "select * from sexo"
            self.data_sexo = self.db.run_select(sql)

            lista_sexo = pd.DataFrame(self.data_sexo)
            
            sql2 = "select * from ciudad"
            self.data_ciudad = self.db.run_select(sql2)
            lista_ciudad = pd.DataFrame(self.data_ciudad)

            self.combo_sexo = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
            self.combo_sexo.place(x = 170, y = 350)
            self.combo_sexo["values"] = lista_sexo[1].tolist()
            self.combo_sexo.bind("<<ComboboxSelected>>", self.prueba)

            self.combo_ciudad = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
            self.combo_ciudad.place(x = 170, y = 400)
            self.combo_ciudad["values"] = lista_ciudad[1].tolist()
            self.combo_ciudad.bind("<<ComboboxSelected>>", self.prueba2)


    
        def prueba(self, event):
            for i in self.data_sexo:
                if self.combo_sexo.get() in i:
                    self.sexo_fin = i[0]
                else:
                    pass
        
        def prueba2(self, event):
            for i in self.data_ciudad:
                if self.combo_ciudad.get() in i:
                    self.ciudad_fin = i[0]
                else:
                    pass
        
        def config_button(self):#Se configura el boton
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.insertar).place(x=0, y =460, width = 200, height = 40)
            tk.Button(self.insert_datos, text = "Cancelar", 
                command = self.insert_datos.destroy).place(x=200, y =460, width = 200, height = 40)

        def insertar(self): #Insercion en la base de datos.
            if len(self.entry_rut.get()) == 0:
                self.warning()
            elif len(self.entry_nombre.get()) == 0:
                self.warning()
            elif len(self.entry_apellido.get()) == 0:
                self.warning()
            elif len(self.cal.get_date()) == 0:
                self.warning()
            else:
                
                sql = ("insert into paciente (rut, nombre_paciente, apellido_paciente" +
                ", fecha_nacimiento, sexo_id_sexo, ciudad_id_ciudad) " +   
                "values (%(rut)s, %(nombre_paciente)s, %(apellido_paciente)s, %(fecha_nacimiento)s"+
                ", %(sexo_id_sexo)s, %(ciudad_id_ciudad)s)")


                self.db.run_sql(sql, {"rut": self.entry_rut.get(), 
                                    "nombre_paciente": self.entry_nombre.get(),
                                    "apellido_paciente": self.entry_apellido.get(),
                                    "fecha_nacimiento": self.cal.get_date(),
                                    "sexo_id_sexo": self.sexo_fin,
                                    "ciudad_id_ciudad": self.ciudad_fin})
                self.insert_datos.destroy()
                self.padre.llenar_treeview_paciente()

        def warning(self):
            messagebox.showwarning(title="Advertencia", message="Uno de los campos ingresados está vacío")

class modificar_paciente:#Clase para modificar
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data =  row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):#Settings
        self.insert_datos.geometry('400x400')
        self.insert_datos.title("Modificar Paciente")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):#Labels
        # tk.Label(self.insert_datos, text =  "RUT: ").place(x = 10, y = 10, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "Apellido: ").place(x = 10, y = 100, width = 130, height = 20)
        # tk.Label(self.insert_datos, text =  "Fecha Nacimiento: ").place(x = 10, y = 150, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Sexo: ").place(x = 10, y = 200, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Ciudad: ").place(x = 10, y = 250, width = 130, height = 20)


    
    def config_entry(self):#Se configuran los inputs

        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 170, y = 10, width = 150, height = 30)
        self.entry_apellido = tk.Entry(self.insert_datos)
        self.entry_apellido.place(x = 170, y = 100, width = 150, height = 30)
        #self.entry_fecha = tk.Entry(self.insert_datos)
        #self.entry_fecha.place(x = 170, y = 150, width = 150, height = 30)


        sql = "select * from sexo"
        self.data_sexo = self.db.run_select(sql)

        lista_sexo = pd.DataFrame(self.data_sexo)
        sql2 = "select * from ciudad"
        self.data_ciudad = self.db.run_select(sql2)
        lista_ciudad = pd.DataFrame(self.data_ciudad)


        self.combo_sexo = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_sexo.place(x = 170, y = 200)
        self.combo_sexo["values"] = lista_sexo[1].tolist()
        self.combo_sexo.bind("<<ComboboxSelected>>", self.prueba)

        self.combo_ciudad = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_ciudad.place(x = 170, y = 250)
        self.combo_ciudad["values"] = lista_ciudad[1].tolist()
        self.combo_ciudad.bind("<<ComboboxSelected>>", self.prueba2)


        lista = pd.DataFrame(self.row_data)
        lista[1].tolist()
        lista[2].tolist()
        lista[3].tolist()
                
        lista_buena = []
        for i in lista[1]:
            lista_buena.append(i)
                
        for i in lista[2]:
            lista_buena.append(i)
    
        for i in lista[3]:
            lista_buena.append(i) 

        self.entry_nombre.insert(0, lista_buena[0])
        self.entry_apellido.insert(0, lista_buena[1])
        # self.entry_fecha.insert(0, lista_buena[2])


    def prueba(self, event):
        print(self.data_sexo)
        print("a : ", self.combo_sexo.get())
        for i in self.data_sexo:
            print(i)
            if self.combo_sexo.get() in i:
                print(i[0])
                self.sexo_fin = i[0]
            else:
                pass
 
    def prueba2(self, event):
        print(self.data_ciudad)
        print("b : ", self.combo_ciudad.get())   
        for i in self.data_ciudad:
            print(i)
            if self.combo_ciudad.get() in i:
                print(i[0])
                self.ciudad_fin = i[0]
            else:
                pass

        
    def config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar", command = self.modificar).place(x=0, y =350, width = 200, height = 40)
        tk.Button(self.insert_datos, text = "Cancelar", command = self.insert_datos.destroy).place(x=200, y =350, width = 200, height = 40)


    def modificar(self): #Insercion en la base de datos. 
        sql = """update paciente set nombre_paciente = %(nombre_paciente)s, apellido_paciente = %(apellido_paciente)s, 
                     sexo_id_sexo = %(sexo_id_sexo)s, ciudad_id_ciudad = %(ciudad_id_ciudad)s
                        where rut = %(rut)s"""

        self.db.run_sql(sql, {"nombre_paciente": self.entry_nombre.get(),
                            "apellido_paciente": self.entry_apellido.get(),
                            "sexo_id_sexo": self.entry_sexo.get(),
                              "ciudad_id_ciudad": self.entry_ciudad.get(),
                              "rut" : self.row_data[0][0]})


        self.insert_datos.destroy()
        self.padre.llenar_treeview_paciente()



programa
