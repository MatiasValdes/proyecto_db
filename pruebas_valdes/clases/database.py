import mysql.connector
from tkinter import messagebox

class Database:
    def __init__(self):
        self.db = None
        self.cursor = None

        try:
            self.db = mysql.connector.connect(host="localhost",
                user="root", passwd="deriva", database="proyecto_bueno")
            self.cursor = self.db.cursor()
            print("Se ha conectado exitosamente")
            
        except mysql.connector.Error as err:
            print("No conecta a la base Unu")
            print(err)
            exit()

    def run_select(self, sql):#Función que corre un select
        #sql se un string con un select en lenguaje sql
        try:
            self.cursor.execute(sql)#ejecuta
            result = self.cursor.fetchall() #Guarda el resultado en result
        except mysql.connector.Error as err:#Si no resulta, avisa
            print("No se pueden obtener los datos")
            print(err)
        return result#Retorna result
        
    def run_select_filter(self, sql, params):
        try:
            self.cursor.execute(sql, params)
            result = self.cursor.fetchall()
        except mysql.connector.Error as err:
            print("No se pueden obtener los datos")
            print(err)
        return result

    def run_sql(self, sql, params):
        try:
            self.cursor.execute(sql, params)
            self.db.commit()
        except mysql.connector.Error as err:
            print("No se puede realizar la sql")
            print(err)
            if (err.args[0]) == 1451:
                self.ventana()
            elif (err.args[0] == 2014):
                self.ventana_2()
    
    class ventana:
        def __init__(self):
            messagebox.showwarning(message="No se puede realizar la eliminación porque este elemento está siendo referenciado por otro elemento", title="Precaución")
    
    class ventana_2:
        def __init__(self):
            messagebox.showwarning(message="No se pueden realizar dos procedimientos en la misma ejecución", title="Precaución")
    