from tkinter.constants import TRUE
from typing import Text
import pandas as pd
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from clases.programa import programa
from tkcalendar import Calendar
from tkinter import messagebox

class paciente_respaldo:
    def __init__(self, root, db):
        self.db = db
        self.data = []
        
        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('500x500')
        self.root.title("Respaldo de pacientes")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_paciente()
        self.config_buttons_paciente()

    def config_treeview_paciente(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3"))
        self.treeview.heading("#0", text = "RUT")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Apellido")
        self.treeview.heading("#3", text = "Fecha Nacimiento")
        self.treeview.column("#0", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#2", minwidth = 275, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 275, width = 250, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 1650)
        self.llenar_treeview_paciente()

    def config_buttons_paciente(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Aceptar", 
                  command = self.root.destroy).place(x = 0, y = 450, width = 250, height = 50)
        tk.Button(self.root, text="Cancelar",
                  command = self.root.destroy).place(x = 250, y = 450, width = 250, height = 50)


    def llenar_treeview_paciente(self):#Se llena el treeview de datos. 
        #sql = ("select paciente.rut, paciente.nombre_paciente, paciente.apellido_paciente, paciente.fecha_nacimiento,"+
        #        "sexo.nombre_sexo, ciudad.nombre_ciudad from paciente inner join sexo on sexo.id_sexo = paciente.sexo_id_sexo "+
        #        "inner join ciudad on ciudad.id_ciudad = paciente.ciudad_id_ciudad")
        sql = "select * from respaldo_paciente"
        # Ejecuta el select
        data = self.db.run_select(sql)
        #print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2],i[3]), iid = i[0])
            self.data = data#Actualiza la data"""
