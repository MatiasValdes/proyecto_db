from os import pathsep
import tkinter as tk
from tkinter import PhotoImage, Toplevel, ttk
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from tkinter import messagebox as MessageBox
from tkinter.constants import LEFT
from PIL import Image, ImageTk
from matplotlib import get_cachedir
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)

class graficos:
    def __init__(self, root, db):
        self.root = root
        self.db = db
        self.root = tk.Toplevel()
        self.root.geometry('600x450')
        self.root.title("Posibilidad de gráficos")
        self.root.resizable(width=0,height=0)

        self.root.transient(root)
        # botones
        self.config_buttons_graficos()
        self.config_combos()
        self.imagen_principal()

    def config_combos(self):
        tk.Label(self.root, text = "Elegir data").place(x=10,y=10, width=100, height=20)
        tk.Label(self.root, text = "Elegir gráfica").place(x=10,y=40, width=100, height=20)
        self.combo = ttk.Combobox(self.root, state="readonly")
        self.combo.place(x=100, y=10)
        self.combo["values"] = ["Pacientes-Ciudad", "Sexo-Vacunados", "Ciudad-Vacunatorio"]

        self.combo_2 = ttk.Combobox(self.root, state="readonly")
        self.combo_2.place(x=100, y=40)
        self.combo_2["values"] = ["Gráfico de barras", "Gráfico de torta"]

        
    def imagen_principal(self):
        frame = LabelFrame(self.root, text = "", relief = tk.FLAT)
        # frame.place(x=300, y=100,relwidth=0.68, relheight=0.95)
        frame.place(x=100, y=100)
        frame.configure(bg="#576CD6")

        image = Image.open("grafico.png")
        photo = ImageTk.PhotoImage(image.resize((400, 300), Image.ANTIALIAS))
        label = Label (frame, image=photo)
        label.image = photo
        label.pack()

    def config_buttons_graficos(self):
        tk.Button(self.root, text="Aceptar", 
                 command = self.menu_seleccionador).place(x = 300, y = 10, width = 70, height = 50)

    
    def menu_seleccionador(self):
        data = self.combo.get()
        data_2 = self.combo_2.get()
        if data_2 == "Gráfico de barras":
            self.grafico_barras(data)

        elif data_2 == "Gráfico de torta":
            self.grafico_pie(data)
        
        else:
            pass


    def grafico_barras(self, data):
        lista = data.split("-")
        print(lista[0])
        self.graph_barras = Toplevel()
        fig, ax = plt.subplots()
        if lista[0] == "Pacientes":
            x, y = self.__get_data()
            text = lista[0] + " x  " + lista[1]
            ax.set_title(text)
            ax.set_ylabel(lista[0])
            ax.set_xlabel(lista[1])
        elif lista[0] == "Sexo":
            x, y = self.__get_data_2()
            ax.set_title("Cantidad de vacunados según el sexo")
            ax.set_ylabel("Total de pacientes")
            ax.set_xlabel("Sexo")
        
        else:
            x, y = self.__get_data_3()
            ax.set_title("Cantidad de vacunatorios en las ciudades")
            ax.set_ylabel("Cantidad de vacunatorios")
            ax.set_xlabel("Ciudad")

        ax.bar(x, y, color = "aqua")
        canvas = FigureCanvasTkAgg(fig, master = self.graph_barras)  
        canvas.draw()
        canvas.get_tk_widget().pack()
    
    
    def grafico_pie(self, data):
        lista = data.split("-")
        
        def parse_data(lista):
            if lista[0] == "Pacientes":
                dataset = self.__get_data()
                dataset = [*dataset]
                return dataset

            elif lista[0] == "Sexo":
                dataset = self.__get_data_2()
                dataset = [*dataset]
                return dataset

            else:
                dataset = self.__get_data_3()
                dataset = [*dataset]
                return dataset
        

        self.graph_pie = Toplevel()
        self.graph_pie.title("Pie")
        dataset = parse_data(lista)
        sizes = dataset[1]
        labels = dataset[0]

        fig, ax = plt.subplots()
        ax.pie(sizes, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)
        ax.axis('equal')
        canvas = FigureCanvasTkAgg(fig, master = self.graph_pie)  
        canvas.draw()
        canvas.get_tk_widget().pack()
        self.graph_pie.mainloop()

   
    def __get_data(self):
        sql = """select ciudad.nombre_ciudad, count(paciente.nombre_paciente) from ciudad
                join paciente on ciudad_id_ciudad = ciudad.id_ciudad 
                group by ciudad.nombre_ciudad;"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y

    def __get_data_2(self):
        sql = """select sexo.nombre_sexo, count(paciente.nombre_paciente) from sexo 
                join paciente on sexo_id_sexo = sexo.id_sexo group by sexo.nombre_sexo;;"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y
    
    def __get_data_3(self):
        sql = """select ciudad.nombre_ciudad, count(vacunatorio.nombre_vacunatorio) from ciudad 
                join vacunatorio on ciudad_id_ciudad = ciudad.id_ciudad 
                group by ciudad.nombre_ciudad;"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y

    def __salir(self, button):
        resultado = MessageBox.askquestion("Salir",
                "¿Está seguro que desea salir?")
        if resultado == "yes":
            self.root.destroy()
        else:
            pass