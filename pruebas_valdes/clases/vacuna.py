import pandas as pd
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as MessageBox


class vacuna:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('876x400')
        self.root.title("Vacunas")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_vacuna()
        self.config_buttons_vacuna()
 
    def config_treeview_vacuna(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Cantidad de dosis")
        self.treeview.heading("#3", text = "Descripción")
        self.treeview.column("#0", minwidth = 50, width = 50, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#2", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#3", minwidth = 275, width = 275, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 876)
        self.llenar_treeview_vacuna()

    def config_buttons_vacuna(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar vacuna", 
                  command = self.llamar).place(x = 0, y = 350, width = 350, height = 50)
        tk.Button(self.root, text="Modificar vacuna",
                  command = self.modif).place(x = 292, y = 350, width = 350, height = 50)
        tk.Button(self.root, text="Eliminar vacuna",
                  command = self.elimin).place(x = 584, y = 350, width = 350, height = 50)

    def llenar_treeview_vacuna(self):#Se llena el treeview de datos. 
        sql = "select * from vacuna"
        print(sql)
        # Ejecuta el select
        data = self.db.run_select(sql)
        print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[3],i[2]), iid = i[0])
            self.data = data#Actualiza la data"""

    def llamar(self):
        print("insertar")
        self.insertar_vacuna(self.db, self)

    def modif(self):
        sql = "select * from vacuna where id_vacuna = %(id_vacuna)s"
        row_data = self.db.run_select_filter(sql, {"id_vacuna": self.treeview.focus()})
        self.modificar_vacuna(self.db, self, row_data)

    def elimin(self):
        sql = "select * from vacuna where id_vacuna = %(id_vacuna)s"
        lista = self.db.run_select_filter(sql, {"id_vacuna": self.treeview.focus()})
        # self.llenar_treeview_vacuna()
        self.warning(lista)
    
    def warning(self, lista):
        for i in lista:
            lista = list(i)
        
        nombre = lista[1]
        # Se obtiene el nombre elegido
        print(nombre)
        
        text = "¿Seguro que desea eliminar la vacuna " + nombre + "?"
        resultado = MessageBox.askquestion("Eliminar", text)
        # Si desea eliminarlo, se ejecuta la opción en mysql
        if resultado == "yes":
            self.elim(nombre)
        else:
            pass

    # Función de eliminación por orden de mysql
    def elim(self, nombre):
        # Eliminación en la base de datos.
        sql = """delete vacuna from vacuna where nombre_vacuna = %(nombre_vacuna)s"""
        # Especificamos el nombre que se quiere eliminar
        self.db.run_sql(sql, {"nombre_vacuna": nombre})
        self.llenar_treeview_vacuna()

    class insertar_vacuna:
        def __init__(self, db, padre):
            self.padre = padre
            self.db = db
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Settings
            self.insert_datos.geometry('350x200')
            self.insert_datos.title("Insertar Vacuna")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Labels
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Descripción: ").place(x = 10, y = 40, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Cantidad de dosis: ").place(x = 10, y = 140, width = 130, height = 20)
    
        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_descripcion = tk.Entry(self.insert_datos)
            self.entry_descripcion.place(x = 170, y = 40, width = 150, height = 70)
            self.entry_dosis = tk.Entry(self.insert_datos)
            self.entry_dosis.place(x = 170, y = 140, width = 150, height = 20)
    
        def config_button(self):#Se configura el boton
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.insertar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar", 
                command = self.insert_datos.destroy).place(x=200, y =170, width = 200, height = 20)

        def insertar(self): #Insercion en la base de datos. 
            sql = "insert into vacuna (nombre_vacuna, descripcion, cant_dosis) values (%(nombre_vacuna)s, %(descripcion)s, %(cant_dosis)s)"
            self.db.run_sql(sql, {"nombre_vacuna": self.entry_nombre.get(), 
                                "descripcion": self.entry_descripcion.get(),
                                  "cant_dosis": self.entry_dosis.get()})
            self.insert_datos.destroy()
            self.padre.llenar_treeview_vacuna()



    class modificar_vacuna:#Clase para modificar
        def __init__(self, db, padre, row_data):
            self.padre = padre
            self.db = db
            self.row_data = row_data
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Configuración de la ventana. 
            self.insert_datos.geometry('400x190')
            self.insert_datos.title("Modificar Vacuna")
            self.insert_datos.resizable(width=0, height=0)
    
        def config_label(self):#Se configuran las etiquetes.
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Descripción: ").place(x = 10, y = 40, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Cantidad de dosis: ").place(x = 10, y = 140, width = 130, height = 20)
    
        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_descripcion = tk.Entry(self.insert_datos)
            self.entry_descripcion.place(x = 170, y = 40, width = 150, height = 70)
            self.entry_dosis = tk.Entry(self.insert_datos)
            self.entry_dosis.place(x = 170, y = 140, width = 150, height = 20)

            lista = pd.DataFrame(self.row_data)
            lista[1].tolist()
            lista[2].tolist()
            lista[3].tolist()
            
            lista_buena = []
            for i in lista[1]:
                lista_buena.append(i)
            
            for i in lista[2]:
                lista_buena.append(i)

            for i in lista[3]:
                lista_buena.append(i)

            # print(lista_buena)

            self.entry_nombre.insert(0, lista_buena[0])
            self.entry_descripcion.insert(0, lista_buena[1])
            self.entry_dosis.insert(0, lista_buena[2])

        def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado. 
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.modificar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar",
                    command = self.insert_datos.destroy).place(x=200, y=170, width=200, height=20)
        def modificar(self): #Insercion en la base de datos. 
            sql = """update vacuna set nombre_vacuna = %(nombre_vacuna)s, cant_dosis = %(cant_dosis)s, descripcion = %(descripcion)s
                    where id_vacuna = %(id_vacuna)s"""

            self.db.run_sql(sql, {"nombre_vacuna": self.entry_nombre.get(), 
                                "descripcion": self.entry_descripcion.get(),
                                "cant_dosis": self.entry_dosis.get(),
                                "id_vacuna" : int(self.row_data[0][0])})

            self.insert_datos.destroy()
            self.padre.llenar_treeview_vacuna()
