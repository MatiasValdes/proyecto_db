import tkinter as tk
from tkinter import Toplevel, messagebox as MessageBox
from tkinter import ttk

import pandas as pd
class sexo:
    def __init__(self, root, db):
        self.db = db
        self.data = []
        
        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('300x300')
        self.root.title("Sexo")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_sexo()
        self.config_buttons_sexo()
 
    def config_treeview_sexo(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.column("#0", minwidth = 50, width = 50, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 250, stretch = False)
        
        self.treeview.place(x = 0, y = 0, height = 350, width = 876)
        self.llenar_treeview_sexo()

    def config_buttons_sexo(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Modificar sexo",
                  command = self.modif).place(x = 0, y = 250, width = 150, height = 50)
        tk.Button(self.root, text="Cancelar",
                  command = self.root.destroy).place(x = 150, y = 250, width = 150, height = 50)



    def llenar_treeview_sexo(self):#Se llena el treeview de datos. 
        sql = "select * from sexo"
        print(sql)
        # Ejecuta el select
        data = self.db.run_select(sql)
        print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1]), iid = i[0])
            self.data = data#Actualiza la data"""


    def modif(self):
        sql = "select * from sexo where id_sexo = %(id_sexo)s"
        row_data = self.db.run_select_filter(sql, {"id_sexo": self.treeview.focus()})
        self.modificar_sexo(self.db, self, row_data)




    class modificar_sexo:
        # Clase para modificar
        def __init__(self, db, padre, row_data):
            self.padre = padre
            self.db = db
            self.row_data = row_data
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Configuración de la ventana.
            self.insert_datos.geometry('400x200')
            self.insert_datos.title("Modificar Sexo")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Se configuran las etiquetes.
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 100, height = 20)
            

        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)

            lista = pd.DataFrame(self.row_data)
            lista[1].tolist()


            lista_buena = []
            for i in lista[1]:
                lista_buena.append(i)

            self.entry_nombre.insert(0, lista_buena[0])
   


        def config_button(self): 
            # Botón aceptar, llama a la función modificar cuando es clickeado.
            tk.Button(self.insert_datos, text = "Aceptar",
                command = self.modificar).place(x=0, y =170, width = 200, height = 30)
            tk.Button(self.insert_datos, text = "Cancelar",
                    command = self.insert_datos.destroy).place(x=200, y=170, width=200, height=30)

        def modificar(self): 
            # Insercion en la base de datos.
            sql = """update sexo set nombre_sexo = %(nombre_sexo)s
                    where id_sexo = %(id_sexo)s"""

            self.db.run_sql(sql, {"nombre_sexo": self.entry_nombre.get(),
                                    "id_sexo" : int(self.row_data[0][0])})

            self.insert_datos.destroy()
            self.padre.llenar_treeview_sexo()