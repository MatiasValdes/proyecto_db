-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: proyecto_bueno
-- ------------------------------------------------------
-- Server version	8.0.27-cluster

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudad` (
  `id_ciudad` int NOT NULL AUTO_INCREMENT,
  `nombre_ciudad` varchar(45) NOT NULL,
  `n_habitantes` int NOT NULL,
  PRIMARY KEY (`id_ciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (4,'talca',200222),(5,'concepción',10);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventario` (
  `vacunatorio_id_vacunatorio` int NOT NULL,
  `vacuna_id_vacuna` int NOT NULL,
  `stock` int DEFAULT NULL,
  PRIMARY KEY (`vacunatorio_id_vacunatorio`,`vacuna_id_vacuna`),
  KEY `fk_vacunatorio_has_vacuna_vacuna1_idx` (`vacuna_id_vacuna`),
  KEY `fk_vacunatorio_has_vacuna_vacunatorio1_idx` (`vacunatorio_id_vacunatorio`),
  CONSTRAINT `fk_vacunatorio_has_vacuna_vacuna1` FOREIGN KEY (`vacuna_id_vacuna`) REFERENCES `vacuna` (`id_vacuna`),
  CONSTRAINT `fk_vacunatorio_has_vacuna_vacunatorio1` FOREIGN KEY (`vacunatorio_id_vacunatorio`) REFERENCES `vacunatorio` (`id_vacunatorio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario`
--

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
INSERT INTO `inventario` VALUES (5,4,12);
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paciente` (
  `rut` varchar(10) NOT NULL,
  `nombre_paciente` varchar(45) NOT NULL,
  `apellido_paciente` varchar(45) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo_id_sexo` int NOT NULL,
  `ciudad_id_ciudad` int NOT NULL,
  PRIMARY KEY (`rut`),
  KEY `fk_paciente_sexo1_idx` (`sexo_id_sexo`),
  KEY `fk_paciente_ciudad1_idx` (`ciudad_id_ciudad`),
  CONSTRAINT `fk_paciente_ciudad1` FOREIGN KEY (`ciudad_id_ciudad`) REFERENCES `ciudad` (`id_ciudad`),
  CONSTRAINT `fk_paciente_sexo1` FOREIGN KEY (`sexo_id_sexo`) REFERENCES `sexo` (`id_sexo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES ('20421324-2','Matias','Vergara','2001-03-02',5,4),('20543032-2','Matias','Valdes','2001-02-01',5,4);
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programa`
--

DROP TABLE IF EXISTS `programa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `programa` (
  `id_programa` int NOT NULL AUTO_INCREMENT,
  `paciente_rut` varchar(10) NOT NULL,
  `fecha_vac` date NOT NULL,
  `vacuna_id_vacuna` int NOT NULL,
  `vacunatorio_id_vacunatorio` int NOT NULL,
  `trabajador_id_trabajador` int NOT NULL,
  `observacion` varchar(9000) DEFAULT NULL,
  PRIMARY KEY (`id_programa`),
  KEY `fk_programa_vacuna1_idx` (`vacuna_id_vacuna`),
  KEY `fk_programa_vacunatorio1_idx` (`vacunatorio_id_vacunatorio`),
  KEY `fk_programa_trabajador1_idx` (`trabajador_id_trabajador`),
  KEY `fk_programa_paciente` (`paciente_rut`),
  CONSTRAINT `fk_programa_paciente` FOREIGN KEY (`paciente_rut`) REFERENCES `paciente` (`rut`),
  CONSTRAINT `fk_programa_trabajador1` FOREIGN KEY (`trabajador_id_trabajador`) REFERENCES `trabajador` (`id_trabajador`),
  CONSTRAINT `fk_programa_vacuna1` FOREIGN KEY (`vacuna_id_vacuna`) REFERENCES `vacuna` (`id_vacuna`),
  CONSTRAINT `fk_programa_vacunatorio1` FOREIGN KEY (`vacunatorio_id_vacunatorio`) REFERENCES `vacunatorio` (`id_vacunatorio`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programa`
--

LOCK TABLES `programa` WRITE;
/*!40000 ALTER TABLE `programa` DISABLE KEYS */;
INSERT INTO `programa` VALUES (1,'20543032-2','2021-12-07',4,5,2,NULL),(2,'20421324-2','2021-04-03',4,5,2,NULL);
/*!40000 ALTER TABLE `programa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sexo`
--

DROP TABLE IF EXISTS `sexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sexo` (
  `id_sexo` int NOT NULL AUTO_INCREMENT,
  `nombre_sexo` varchar(45) NOT NULL,
  PRIMARY KEY (`id_sexo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sexo`
--

LOCK TABLES `sexo` WRITE;
/*!40000 ALTER TABLE `sexo` DISABLE KEYS */;
INSERT INTO `sexo` VALUES (5,'Masculino'),(6,'Femenino');
/*!40000 ALTER TABLE `sexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabajador`
--

DROP TABLE IF EXISTS `trabajador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trabajador` (
  `id_trabajador` int NOT NULL AUTO_INCREMENT,
  `nombre_trabajador` varchar(45) NOT NULL,
  `apellido_trabajador` varchar(45) NOT NULL,
  PRIMARY KEY (`id_trabajador`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabajador`
--

LOCK TABLES `trabajador` WRITE;
/*!40000 ALTER TABLE `trabajador` DISABLE KEYS */;
INSERT INTO `trabajador` VALUES (2,'Lukas','Copec');
/*!40000 ALTER TABLE `trabajador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacuna`
--

DROP TABLE IF EXISTS `vacuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacuna` (
  `id_vacuna` int NOT NULL AUTO_INCREMENT,
  `nombre_vacuna` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `cant_dosis` int NOT NULL,
  PRIMARY KEY (`id_vacuna`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacuna`
--

LOCK TABLES `vacuna` WRITE;
/*!40000 ALTER TABLE `vacuna` DISABLE KEYS */;
INSERT INTO `vacuna` VALUES (4,'sinovac','vacuna de las maoma',3);
/*!40000 ALTER TABLE `vacuna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacunatorio`
--

DROP TABLE IF EXISTS `vacunatorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacunatorio` (
  `id_vacunatorio` int NOT NULL AUTO_INCREMENT,
  `nombre_vacunatorio` varchar(45) NOT NULL,
  `aforo` int NOT NULL,
  `ciudad_id_ciudad` int NOT NULL,
  PRIMARY KEY (`id_vacunatorio`),
  KEY `fk_vacunatorio_ciudad1_idx` (`ciudad_id_ciudad`),
  CONSTRAINT `fk_vacunatorio_ciudad1` FOREIGN KEY (`ciudad_id_ciudad`) REFERENCES `ciudad` (`id_ciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacunatorio`
--

LOCK TABLES `vacunatorio` WRITE;
/*!40000 ALTER TABLE `vacunatorio` DISABLE KEYS */;
INSERT INTO `vacunatorio` VALUES (5,'Culenar',1502,4),(6,'la florida',210,5);
/*!40000 ALTER TABLE `vacunatorio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-09 11:57:54
