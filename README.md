# Proyecto_DB

Proyecto base de datos
Matías Valdés
Matías Vergara

## Requisitos

* Python3
* Mysql
* Tkinter


## Librería de python utilizadas

```
mysql
matplotlib
PIL
Tkinter
```

## Instalación de librerías

* pip install mysql-connector-python
* pip install matplotlib
* pip install PIL
* pip install sklearn
* pip install pandas
* sudo apt-get update
* sudo apt install python3-tk



## Ejecución

- Dirigirse a la carpeta y utilizar
* python3 main.py

