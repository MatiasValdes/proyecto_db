import tkinter as tk
from tkinter import PhotoImage, Toplevel, ttk
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from tkinter import messagebox as MessageBox
from tkinter.constants import LEFT
from PIL import Image, ImageTk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)

class graficos:
    def __init__(self, root, db):
        self.root = root
        self.db = db
        self.root = tk.Toplevel()
        self.root.geometry('600x450')
        self.root.title("Posibilidad de gráficos")
        self.root.resizable(width=0,height=0)

        self.root.transient(root)
        # botones
        self.config_buttons_graficos()
        self.config_combos()
        self.imagen_principal()

    def config_combos(self):
        tk.Label(self.root, text = "Elegir data").place(x=10,y=10, width=100, height=20)
        tk.Label(self.root, text = "Elegir gráfica").place(x=10,y=40, width=100, height=20)
        self.combo = ttk.Combobox(self.root, state="readonly")
        self.combo.place(x=100, y=10)
        self.combo["values"] = ["Pacientes x Ciudad", "Sexo x Vacunados", "Sexo x Ciudad", "Vacuna x Vacunados"]

        self.combo_2 = ttk.Combobox(self.root, state="readonly")
        self.combo_2.place(x=100, y=40)
        self.combo_2["values"] = ["Gráfico de barras", "Gráfico de torta", "Gráfico de dispersión"]

        
    def imagen_principal(self):
        frame = LabelFrame(self.root, text = "", relief = tk.FLAT)
        # frame.place(x=300, y=100,relwidth=0.68, relheight=0.95)
        frame.place(x=100, y=100)
        frame.configure(bg="#576CD6")

        image = Image.open("grafico.png")
        photo = ImageTk.PhotoImage(image.resize((400, 300), Image.ANTIALIAS))
        label = Label (frame, image=photo)
        label.image = photo
        label.pack()

    def config_buttons_graficos(self):
        tk.Button(self.root, text="Aceptar", 
                 command = self.menu_seleccionador).place(x = 300, y = 10, width = 70, height = 50)

    
    def menu_seleccionador(self):
        data = self.combo.get()
        data_2 = self.combo_2.get()
        print(data, data_2)  

    def grafico_barras(self):
        self.graph_barras = Toplevel()
        fig, ax = plt.subplots()
        x, y = self.__get_data()
        ax.set_title('Número de jugadores por equipo')
        ax.set_ylabel("Sexo")
        ax.set_xlabel("Cantidad de vacunados")
        ax.bar(x, y, color = "salmon")
        canvas = FigureCanvasTkAgg(fig, master = self.graph_barras)  
        canvas.draw()
        canvas.get_tk_widget().pack()
    
    def __get_data(self):
        sql = """select equipo.nom_equipo, count(jugador.nom_jugador) from equipo
                join jugador on jugador.id_equipo = equipo.id_equipo 
                group by equipo.nom_equipo;"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y

    def __salir(self, button):
        resultado = MessageBox.askquestion("Salir",
                "¿Está seguro que desea salir?")
        if resultado == "yes":
            self.root.destroy()
        else:
            pass