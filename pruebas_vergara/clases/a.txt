
"""
class eliminar_inventario:
        # Clase para modificar
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_combo()
            # self.config_entry()
        self.config_button()

    def config_window(self):#Configuración de la ventana.
        self.insert_datos.geometry('400x200')
        self.insert_datos.title("Eliminar Stock Vacuna")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):#Se configuran las etiquetes.
        tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 100, height = 20)
        # tk.Label(self.insert_datos, text =  "Numero de habitantes: ").pla
        # ce(x = 10, y = 40, width = 100, height = 20)
            

    def config_combo(self):
        # Se configura el combobox
        lista = pd.DataFrame(self.row_data)
        lista[1].tolist()
        lista[2].tolist()

        lista_buena = []
        for i in lista[1]:
            lista_buena.append(i)

        self.combo = ttk.Combobox(self.insert_datos, state = "readonly")
        self.combo.place(x = 110, y = 10)
        self.combo["values"] = lista_buena

    # Se configuran los botones
    def config_button(self): 
        # Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.warning).place(x=0, y =170, width = 200, height = 20)
        tk.Button(self.insert_datos, text = "Cancelar",
                command = self.insert_datos.destroy).place(x = 200, 
                        y = 170, width = 200, height =20)

    # Mensaje de precaución
    def warning(self):
        # Se obtiene el nombre elegido
        nombre = self.combo.get()
        text = "¿Seguro que desea eliminar " + nombre + "?"
        resultado = MessageBox.askquestion("Eliminar", text)
        # Si desea eliminarlo, se ejecuta la opción en mysql
        if resultado == "yes":
            self.elim(nombre)
        else:
            pass

    # Función de eliminación por orden de mysql
    def elim(self, nombre):
        # Eliminación en la base de datos.
        sql = """delete ciudad from ciudad where nombre_ciudad = %(nombre_ciudad)s"""
        # Especificamos el nombre que se quiere eliminar
        self.db.run_sql(sql, {"nombre_ciudad": nombre})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_ciudad()"""
