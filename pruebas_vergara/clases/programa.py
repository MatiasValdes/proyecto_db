import tkinter as tk
import pandas as pd
import datetime
from tkinter import ttk

class programa:
    def __init__(self, root, db, consulta):
        self.db = db
        self.data = []
        self.consulta = consulta

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('1700x800')
        self.root.title("Programa Pacientes")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_programa()
        self.config_buttons_programa()

    def config_treeview_programa(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3", "#4", "#5", "#6"))
        self.treeview.heading("#0", text = "ID")
        self.treeview.heading("#1", text = "RUT Paciente")
        self.treeview.heading("#2", text = "Fecha")
        self.treeview.heading("#3", text = "Vacuna")
        self.treeview.heading("#4", text = "Vacunatorio")
        self.treeview.heading("#5", text = "Nombre Trabajador")
        self.treeview.heading("#6", text = "Apellido Trabajador")
        self.treeview.column("#0", minwidth = 50, width = 60, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#2", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#3", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#4", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#5", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#6", minwidth = 275, width = 275, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 1700)
        self.llenar_treeview_programa()

    def config_buttons_programa(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar Programa", 
                  command = self.llamar).place(x = 0, y = 750, width = 458, height = 50)
        tk.Button(self.root, text="Modificar Programa",
                  command = self.modif).place(x = 458, y = 750, width = 458, height = 50)
        tk.Button(self.root, text="Eliminar Programa",
                  command = self.elim).place(x = 916, y = 750, width = 458, height = 50)

    def llenar_treeview_programa(self):#Se llena el treeview de datos. 
        #sql = ("select * from programa")
        sql = ("""select programa.id_programa, programa.paciente_rut, programa.fecha_vac, vacuna.nombre_vacuna, 
        vacunatorio.nombre_vacunatorio, trabajador.nombre_trabajador, trabajador.apellido_trabajador from programa 
        inner join vacuna on programa.vacuna_id_vacuna = vacuna.id_vacuna 
        inner join vacunatorio on vacunatorio.id_vacunatorio = programa.vacunatorio_id_vacunatorio 
        inner join trabajador on trabajador.id_trabajador = programa.trabajador_id_trabajador where programa.paciente_rut = '""" + self.consulta + "'")
        #sql = ("select * from programa where paciente_rut = '" + self.consulta + "'")
        print(sql)
        
        # Ejecuta el select
        data = self.db.run_select(sql)
        print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2],i[3],i[4],i[5], i[6]), iid = i[0])
            self.data = data#Actualiza la data"""

    def llamar(self):
        print("insertar")
        insertar_programa(self.db, self, self.consulta)

    def modif(self):
        print("modificar")
        sql = "select * from programa where id_programa = %(id_programa)s"
        row_data = self.db.run_select_filter(sql, {"id_programa": self.treeview.focus()})
        modificar_programa(self.db, self, row_data, self.consulta)

    def elim(self):
        print("eliminar")
        sql = "delete from programa where id_programa = %(id_programa)s"
        self.db.run_sql(sql, {"id_programa": self.treeview.focus()})
        self.llenar_treeview_programa()

class insertar_programa:
    def __init__(self, db, padre, consulta):
        self.padre = padre
        self.db = db
        self.consulta = consulta
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):#Settings
        self.insert_datos.geometry('400x760')
        self.insert_datos.title("Insertar Programa")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):#Labels
        tk.Label(self.insert_datos, text =  "Fecha: ").place(x = 10, y = 10, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Vacuna ").place(x = 10, y = 143, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Vacunatorio: ").place(x = 10, y = 276, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Trabajador: ").place(x = 10, y = 409, width = 130, height = 20)
    
    def config_entry(self):#Se configuran los inputs
        self.ahora = datetime.datetime.now()
        self.ahora = self.ahora.strftime("%Y-%m-%d")
        self.entry_fecha = tk.Entry(self.insert_datos)
        self.entry_fecha.place(x = 170, y = 10, width = 150, height = 30)

        sql = "select * from vacuna"
        self.data_vacuna = self.db.run_select(sql)
        self.entry_fecha.insert(0, self.ahora)
#            print(self.data_sexo)

        lista_vacuna = pd.DataFrame(self.data_vacuna)

        sql2 = "select * from vacunatorio"
        self.data_vacunatorio = self.db.run_select(sql2)
#        print(self.data_ciudad)
        lista_vacunatorio = pd.DataFrame(self.data_vacunatorio)

        sql3 = "select * from trabajador"
        self.data_trabajador = self.db.run_select(sql3)
#            print(self.data_ciudad)
        lista_trabajador = pd.DataFrame(self.data_trabajador)

#            print(lista_sexo[1].tolist())
#            print(lista_ciudad[1].tolist())

        self.combo_vacuna = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_vacuna.place(x = 170, y = 143)
        self.combo_vacuna["values"] = lista_vacuna[1].tolist()
        self.combo_vacuna.bind("<<ComboboxSelected>>", self.prueba)

        self.combo_vacunatorio = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_vacunatorio.place(x = 170, y = 276)
        self.combo_vacunatorio["values"] = lista_vacunatorio[1].tolist()
        self.combo_vacunatorio.bind("<<ComboboxSelected>>", self.prueba2)

        list_trab = (lista_trabajador[1] + lista_trabajador[2])
        self.combo_trabajador = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_trabajador.place(x = 170, y = 409)
        self.combo_trabajador["values"] = list_trab.tolist()
        self.combo_trabajador.bind("<<ComboboxSelected>>", self.prueba3)

    def prueba(self, event):
        print(self.data_vacuna)
        print("a : ", self.combo_vacuna.get())
        for i in self.data_vacuna:
            print(i)
            if self.combo_vacuna.get() in i:
                print(i[0])
                self.vacuna_fin = i[0]
            else:
                pass
 
    def prueba2(self, event):
        print(self.data_vacunatorio)
        print("b : ", self.combo_vacunatorio.get())   
        for i in self.data_vacunatorio:
            print(i)
            if self.combo_vacunatorio.get() in i:
                print(i[0])
                self.vacunatorio_fin = i[0]
            else:
                pass

    def prueba3(self, event):
        print(self.data_trabajador)
        print("c : ", self.combo_trabajador.get())   
        print(self.data_trabajador)
        for i in self.data_trabajador:
            print(i)
            print(self.combo_trabajador.get())
            if self.combo_trabajador.get() in i:
                print(i[0], "aaaaaaaaaaaaaaaaaaaa")
                self.trabajador_fin = i[0]
            else:
                pass
    
    def config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar", 
            command = self.insertar).place(x=0, y =720, width = 200, height = 40)
        tk.Button(self.insert_datos, text = "Cancelar", 
            command = self.insert_datos.destroy).place(x=200, y =720, width = 200, height = 40)

    def insertar(self): #Insercion en la base de datos.
        sql = "SET FOREIGN_KEY_CHECKS=0;"
        self.db.run_sql(sql, None)

        sql = """insert into programa(fecha_vac, paciente_rut, vacuna_id_vacuna, vacunatorio_id_vacunatorio, trabajador_id_trabajador) 
        values (%(fecha_vac)s, %(paciente_rut)s, %(vacuna_id_vacuna)s, %(vacunatorio_id_vacunatorio)s, %(trabajador_id_trabajador)s)"""

        self.db.run_sql(sql, {"fecha_vac": self.ahora, 
                            "paciente_rut": self.consulta,
                            "vacuna_id_vacuna": self.vacuna_fin,
                            "vacunatorio_id_vacunatorio": self.vacunatorio_fin,
                            "trabajador_id_trabajador": self.trabajador_fin})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_programa()



class modificar_programa:#Clase para modificar
    def __init__(self, db, padre, row_data, consulta):
        self.padre = padre
        self.db = db
        self.row_data =  row_data
        self.consulta = consulta
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):#Settings
        self.insert_datos.geometry('400x760')
        self.insert_datos.title("Modificar Programa")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):#Labels
        tk.Label(self.insert_datos, text =  "Fecha: ").place(x = 10, y = 10, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Vacuna ").place(x = 10, y = 143, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Vacunatorio: ").place(x = 10, y = 276, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "ID Trabajador: ").place(x = 10, y = 409, width = 130, height = 20)    
    
    def config_entry(self):#Se configuran los inputs

        sql = "select * from vacuna"
        self.data_vacuna = self.db.run_select(sql)
#            print(self.data_sexo)

        lista_vacuna = pd.DataFrame(self.data_vacuna)

        sql2 = "select * from vacunatorio"
        self.data_vacunatorio = self.db.run_select(sql2)
#            print(self.data_ciudad)
        lista_vacunatorio = pd.DataFrame(self.data_vacunatorio)

        sql3 = "select * from trabajador"
        self.data_trabajador = self.db.run_select(sql3)
#            print(self.data_ciudad)
        lista_trabajador = pd.DataFrame(self.data_trabajador)
 
        self.entry_fecha = tk.Entry(self.insert_datos)
        self.entry_fecha.place(x = 170, y = 10, width = 150, height = 30) 
        self.entry_fecha.insert(0, self.row_data[0][2])
        list_trab = (lista_trabajador[1] + lista_trabajador[2])
        print(list_trab.tolist())
        self.combo_vacuna = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_vacuna.place(x = 170, y = 143)
        self.combo_vacuna["values"] = lista_vacuna[1].tolist()
        self.combo_vacuna.bind("<<ComboboxSelected>>", self.prueba)

        self.combo_vacunatorio = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_vacunatorio.place(x = 170, y = 276)
        self.combo_vacunatorio["values"] = lista_vacunatorio[1].tolist()
        self.combo_vacunatorio.bind("<<ComboboxSelected>>", self.prueba2)

        self.combo_trabajador = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_trabajador.place(x = 170, y = 409)
        self.combo_trabajador["values"] = list_trab.tolist()
        self.combo_trabajador.bind("<<ComboboxSelected>>", self.prueba3)
 
    def prueba(self, event):
        print(self.data_vacuna)
        print("a : ", self.combo_vacuna.get())
        for i in self.data_vacuna:
            print(i)
            if self.combo_vacuna.get() in i:
                print(i[0])
                self.vacuna_finn = i[0]
            else:
                pass
 
    def prueba2(self, event):
        print(self.data_vacunatorio)
        print("b : ", self.combo_vacunatorio.get())   
        for i in self.data_vacunatorio:
            print(i)
            if self.combo_vacunatorio.get() in i:
                print(i[0])
                self.vacunatorio_finn = i[0]
            else:
                pass

    def prueba3(self, event):
        print(self.data_trabajador)
        print("b : ", self.combo_trabajador.get())   
        for i in self.data_trabajador:
            print(i)
            if self.combo_trabajador.get() in i:
                print(i[0])
                self.trabajador_finn = i[0]
            else:
                pass       
    def config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar", command = self.modificar).place(x=0, y =720, width = 200, height = 40)
        tk.Button(self.insert_datos, text = "Cancelar", command = self.insert_datos.destroy).place(x=200, y=720, width=200, height=40)


    def modificar(self): #Insercion en la base de datos. 
        sql = """update programa set paciente_rut = %(paciente_rut)s, fecha_vac = CAST(%(fecha_vac)s AS DATE),
        vacuna_id_vacuna = %(vacuna_id_vacuna)s, vacunatorio_id_vacunatorio = %(vacunatorio_id_vacunatorio)s, trabajador_id_trabajador = %(trabajador_id_trabajador)s 
        where id_programa = %(id_programa)s"""

        self.db.run_sql(sql, {"paciente_rut": self.consulta,
                            "fecha_vac": self.entry_fecha.get(),
                              "vacuna_id_vacuna": self.vacuna_finn,
                            "vacunatorio_id_vacunatorio": self.vacunatorio_finn,
                              "trabajador_id_trabajador": self.trabajador_finn,
                              "id_programa" : self.row_data[0][0]})


        self.insert_datos.destroy()
        self.padre.llenar_treeview_programa()

