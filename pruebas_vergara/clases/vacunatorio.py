import tkinter as tk
import pandas as pd
from tkinter import ttk
from clases.inventario import inventario


class vacunatorio:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('1168x800')
        self.root.title("Vacunatorios")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_vacunatorio()
        self.config_buttons_vacunatorio()
 
    def config_treeview_vacunatorio(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Aforo")
        self.treeview.heading("#3", text = "Ciudad")
        self.treeview.column("#0", minwidth = 50, width = 50, stretch = False)
        self.treeview.column("#1", minwidth = 372, width = 372, stretch = False)
        self.treeview.column("#2", minwidth = 372, width = 372, stretch = False)
        self.treeview.column("#3", minwidth = 372, width = 372, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 1168)
        self.llenar_treeview_vacunatorio()

    def config_buttons_vacunatorio(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar Vacunatorio", 
                  command = self.llamar).place(x = 0, y = 750, width = 292, height = 50)
        tk.Button(self.root, text="Modificar Vacunatorio",
                  command = self.modif).place(x = 292, y = 750, width = 292, height = 50)
        tk.Button(self.root, text="Eliminar Vacunatorio",
                  command = self.elimin).place(x = 584, y = 750, width = 292, height = 50)
        tk.Button(self.root, text="Stock Vacunatorio",
                  command = self.invent, fg = "red", bg="#d7f7ff").place(x = 876, y = 750, width = 292, height = 50)

    def llenar_treeview_vacunatorio(self):#Se llena el treeview de datos. 
        sql = """select vacunatorio.id_vacunatorio, vacunatorio.nombre_vacunatorio, 
            vacunatorio.aforo, ciudad.nombre_ciudad from vacunatorio 
            inner join ciudad on vacunatorio.ciudad_id_ciudad = ciudad.id_ciudad"""

        # Ejecuta el select
        data = self.db.run_select(sql)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2],i[3]), iid = i[0])
            self.data = data#Actualiza la data"""

    def llamar(self):
        self.insertar_vacunatorio(self.db, self)

    def modif(self):
        sql = "select * from vacunatorio where id_vacunatorio = %(id_vacunatorio)s"
        row_data = self.db.run_select_filter(sql, {"id_vacunatorio": self.treeview.focus()})
        self.modificar_vacunatorio(self.db, self, row_data)

    def elimin(self):
        sql = "delete from vacunatorio where id_vacunatorio = %(id_vacunatorio)s"
        self.db.run_sql(sql, {"id_vacunatorio": self.treeview.focus()})
        self.llenar_treeview_vacunatorio()

    def invent(self):
        sql = "select * from vacunatorio where id_vacunatorio = %(id_vacunatorio)s"
        row_data = self.db.run_select_filter(sql, {"id_vacunatorio": self.treeview.focus()})
        print(row_data, "data vacunatorio where")
        mostrar = row_data[0][0]

        inventario(self.root, self.db, mostrar)


    class insertar_vacunatorio:
        def __init__(self, db, padre):
            self.padre = padre
            self.db = db
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Settings
            self.insert_datos.geometry('400x190')
            self.insert_datos.title("Insertar Vacunatorio")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Labels
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Aforo: ").place(x = 10, y = 40, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "ID Ciudad: ").place(x = 10, y = 140, width = 130, height = 20)
    
        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_aforo = tk.Entry(self.insert_datos)
            self.entry_aforo.place(x = 170, y = 40, width = 150, height = 70)

            sql2 = "select * from ciudad"
            self.data_ciudad = self.db.run_select(sql2)
#            print(self.data_ciudad)
            lista_ciudad = pd.DataFrame(self.data_ciudad)

            self.combo_ciudad = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
            self.combo_ciudad.place(x = 170, y = 140)
            self.combo_ciudad["values"] = lista_ciudad[1].tolist()
            self.combo_ciudad.bind("<<ComboboxSelected>>", self.prueba2)

        def prueba2(self, event):
            print(self.data_ciudad)
            print("b : ", self.combo_ciudad.get())   
            for i in self.data_ciudad:
                print(i)
                if self.combo_ciudad.get() in i:
                    print(i[0])
                    self.ciudad_fin = i[0]
                else:
                    pass

        def config_button(self):#Se configura el boton
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.insertar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar", 
                command = self.insert_datos.destroy).place(x=200, y =170, width = 200, height = 20)

        def insertar(self): #Insercion en la base de datos. 
            sql = "insert into vacunatorio (nombre_vacunatorio, aforo, ciudad_id_ciudad) values (%(nombre_vacunatorio)s, %(aforo)s, %(ciudad_id_ciudad)s)"
            self.db.run_sql(sql, {"nombre_vacunatorio": self.entry_nombre.get(), 
                                "aforo": self.entry_aforo.get(),
                                  "ciudad_id_ciudad": self.ciudad_fin})
            self.insert_datos.destroy()
            self.padre.llenar_treeview_vacunatorio()


    class modificar_vacunatorio:#Clase para modificar
        def __init__(self, db, padre, row_data):
            self.padre = padre
            self.db = db
            self.row_data = row_data
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Configuración de la ventana. 
            self.insert_datos.geometry('400x190')
            self.insert_datos.title("Modificar Vacunatorio")
            self.insert_datos.resizable(width=0, height=0)
    
        def config_label(self):#Se configuran las etiquetes.
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Aforo: ").place(x = 10, y = 40, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "ID Ciudad: ").place(x = 10, y = 140, width = 130, height = 20)
    
        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_aforo = tk.Entry(self.insert_datos)
            self.entry_aforo.place(x = 170, y = 40, width = 150, height = 70)

            sql2 = "select * from ciudad"
            self.data_ciudad = self.db.run_select(sql2)
#            print(self.data_ciudad)
            lista_ciudad = pd.DataFrame(self.data_ciudad)

            self.combo_ciudad = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
            self.combo_ciudad.place(x = 170, y = 140)
            self.combo_ciudad["values"] = lista_ciudad[1].tolist()
            self.combo_ciudad.bind("<<ComboboxSelected>>", self.prueba2)

            self.entry_nombre.insert(0, self.row_data[0][1])
            self.entry_aforo.insert(0, self.row_data[0][2])

        def prueba2(self, event):
            print(self.data_ciudad)
            print("b : ", self.combo_ciudad.get())   
            for i in self.data_ciudad:
                print(i)
                if self.combo_ciudad.get() in i:
                    print(i[0])
                    self.ciudad_fin = i[0]
                else:
                    pass

        def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado. 
            tk.Button(self.insert_datos, text = "Aceptar", 
                command = self.modificar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar",
                    command = self.insert_datos.destroy).place(x=200, y=170, width=200, height=20)

        def modificar(self): #Insercion en la base de datos. 
            sql = """update vacunatorio set nombre_vacunatorio = %(nombre_vacunatorio)s, aforo = %(aforo)s, ciudad_id_ciudad = %(ciudad_id_ciudad)s
                    where id_vacunatorio = %(id_vacunatorio)s"""

            self.db.run_sql(sql, {"nombre_vacunatorio": self.entry_nombre.get(), 
                                "aforo": self.entry_aforo.get(),
                                "ciudad_id_ciudad": self.entry_ciudad.get(),
                                "id_vacunatorio" : int(self.row_data[0][0])})

            self.insert_datos.destroy()
            self.padre.llenar_treeview_vacunatorio()
