import tkinter as tk
from tkinter import messagebox as MessageBox
from tkinter import ttk

import pandas as pd

class ciudad:
    def __init__(self, root, db):
        self.db = db
        self.data = []
        
        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('600x400')
        self.root.title("Ciudades")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_ciudad()
        self.config_buttons_ciudad()
 
    def config_treeview_ciudad(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Numero de habitantes")
        self.treeview.column("#0", minwidth = 50, width = 50, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 270, stretch = False)
        self.treeview.column("#2", minwidth = 275, width = 290, stretch = False)
        
        self.treeview.place(x = 0, y = 0, height = 350, width = 876)
        self.llenar_treeview_ciudad()

    def config_buttons_ciudad(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar ciudad",
                  command = self.insert_ciudad).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar ciudad",
                  command = self.modif).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar ciudad",
                  command = self.eliminar).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_ciudad(self):#Se llena el treeview de datos. 
        sql = "select * from ciudad"
        print(sql)
        # Ejecuta el select
        data = self.db.run_select(sql)
        print(data)
        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                print(i)
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0], values = (i[1],i[2]), iid = i[0])
            self.data = data#Actualiza la data"""

    def insert_ciudad(self):
        self.insertar_ciudad(self.db, self)

    def modif(self):
        sql = "select * from ciudad where id_ciudad = %(id_ciudad)s"
        row_data = self.db.run_select_filter(sql, {"id_ciudad": self.treeview.focus()})
        self.modificar_ciudad(self.db, self, row_data)

    def eliminar(self):
        sql = "select * from ciudad where id_ciudad = %(id_ciudad)s"
        lista = self.db.run_select_filter(sql, {"id_ciudad": self.treeview.focus()})
        # row_data = self.db.run_select(sql)
        self.warning(lista)
        # Mensaje de precaución
    
    def warning(self, lista):
        for i in lista:
            lista = list(i)
        
        nombre = lista[1]
        # Se obtiene el nombre elegido
        print(nombre)
        
        text = "¿Seguro que desea eliminar " + nombre + "?"
        resultado = MessageBox.askquestion("Eliminar", text)
        # Si desea eliminarlo, se ejecuta la opción en mysql
        if resultado == "yes":
            self.elim(nombre)
        else:
            pass

    # Función de eliminación por orden de mysql
    def elim(self, nombre):
        # Eliminación en la base de datos.
        sql = """delete ciudad from ciudad where nombre_ciudad = %(nombre_ciudad)s"""
        # Especificamos el nombre que se quiere eliminar
        self.db.run_sql(sql, {"nombre_ciudad": nombre})
        self.llenar_treeview_ciudad()    


    # Clase para insertar ciudad
    class insertar_ciudad:
        def __init__(self, db, padre):
            self.padre = padre
            self.db = db
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Settings
            self.insert_datos.geometry('410x190')
            self.insert_datos.title("Insertar Ciudad")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Labels
            tk.Label(self.insert_datos, text =  "Nombre Ciudad: ").place(x = 10, y = 10, width = 130, height = 20)
            tk.Label(self.insert_datos, text =  "Numero de Habitantes: ").place(x = 10, y = 40, width = 130, height = 20)

        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_habitantes = tk.Entry(self.insert_datos)
            self.entry_habitantes.place(x = 170, y = 40, width = 150, height = 40)
    
        def config_button(self):#Se configura el boton
            tk.Button(self.insert_datos, text = "Aceptar",
                command = self.insertar).place(x=0, y =170, width = 205, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar",
                    command=self.insert_datos.destroy).place(x=205, y=170, width=200, height=20)

        def insertar(self): #Insercion en la base de datos. 
            sql = "insert into ciudad (nombre_ciudad, n_habitantes) values (%(nombre_ciudad)s, %(n_habitantes)s)"
            self.db.run_sql(sql, {"nombre_ciudad": self.entry_nombre.get(), 
                                "n_habitantes": self.entry_habitantes.get(),})
            self.insert_datos.destroy()
            self.padre.llenar_treeview_ciudad()


    class modificar_ciudad:
        # Clase para modificar
        def __init__(self, db, padre, row_data):
            self.padre = padre
            self.db = db
            self.row_data = row_data
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_entry()
            self.config_button()

        def config_window(self):#Configuración de la ventana.
            self.insert_datos.geometry('400x200')
            self.insert_datos.title("Modificar Ciudad")
            self.insert_datos.resizable(width=0, height=0)

        def config_label(self):#Se configuran las etiquetes.
            tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 100, height = 20)
            tk.Label(self.insert_datos, text =  "Numero de habitantes: ").place(x = 10, y = 40, width = 100, height = 20)
            

        def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 170, y = 10, width = 150, height = 20)
            self.entry_habitantes = tk.Entry(self.insert_datos)
            self.entry_habitantes.place(x = 170, y = 40, width = 150, height = 70)

            lista = pd.DataFrame(self.row_data)
            lista[1].tolist()
            lista[2].tolist()

            lista_buena = []
            for i in lista[1]:
                lista_buena.append(i)

            for i in lista[2]:
                lista_buena.append(i)

            # print(lista_buena)

            self.entry_nombre.insert(0, lista_buena[0])
            self.entry_habitantes.insert(0, lista_buena[1])


        def config_button(self): 
            # Botón aceptar, llama a la función modificar cuando es clickeado.
            tk.Button(self.insert_datos, text = "Aceptar",
                command = self.modificar).place(x=0, y =170, width = 200, height = 20)
            tk.Button(self.insert_datos, text = "Cancelar",
                    command = self.insert_datos.destroy).place(x=200, y=170, width=200, height=20)

        def modificar(self): 
            # Insercion en la base de datos.
            sql = """update ciudad set nombre_ciudad = %(nombre_ciudad)s, n_habitantes = %(n_habitantes)s
                    where id_ciudad = %(id_ciudad)s"""

            self.db.run_sql(sql, {"nombre_ciudad": self.entry_nombre.get(),
                                    "n_habitantes": self.entry_habitantes.get(),
                                    "id_ciudad" : int(self.row_data[0][0])})

            self.insert_datos.destroy()
            self.padre.llenar_treeview_ciudad()

    class eliminar_ciudad:
        # Clase para modificar
        def __init__(self, db, padre, row_data):
            self.padre = padre
            self.db = db
            self.lista = row_data
            self.insert_datos = tk.Toplevel()
            self.config_window()
            self.config_label()
            self.config_combo()
            # self.config_entry()
            self.config_button()

        # Mensaje de precaución
        def warning(self):
            # Se obtiene el nombre elegido
            print(self.lista)
            """
            text = "¿Seguro que desea eliminar " + lista + "?"
            resultado = MessageBox.askquestion("Eliminar", text)
            # Si desea eliminarlo, se ejecuta la opción en mysql
            if resultado == "yes":
                self.elim(nombre)
            else:
                pass

        # Función de eliminación por orden de mysql
        def elim(self, nombre):
            # Eliminación en la base de datos.
            sql = """#delete ciudad from ciudad where nombre_ciudad = %(nombre_ciudad)s"""
            # Especificamos el nombre que se quiere eliminar
            #self.db.run_sql(sql, {"nombre_ciudad": nombre})

            #self.insert_datos.destroy()
            #self.padre.llenar_treeview_ciudad()

