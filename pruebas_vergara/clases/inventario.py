import tkinter as tk
from tkinter import ttk
import pandas as pd

class inventario:
    def __init__(self, root, db, consulta):
        self.db = db
        self.data = []
        self.consulta = consulta

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('825x800')
        self.root.title("Inventario Vacunas")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.config_treeview_inventario()
        self.config_buttons_inventario()

    def config_treeview_inventario(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1"))
        self.treeview.heading("#0", text = "Vacuna")
        self.treeview.heading("#1", text = "Stock")
        self.treeview.column("#0", minwidth = 275, width = 275, stretch = False)
        self.treeview.column("#1", minwidth = 275, width = 275, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 800, width = 875)
        self.llenar_treeview_inventario()

    def config_buttons_inventario(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar Inventario", 
                  command = self.llamar).place(x = 0, y = 750, width = 275, height = 50)
        tk.Button(self.root, text="Modificar Inventario",
                  command = self.modif).place(x = 275, y = 750, width = 275, height = 50)
        tk.Button(self.root, text="Eliminar Inventario",
                  command = self.elim).place(x = 550, y = 750, width = 275, height = 50)

    def llenar_treeview_inventario(self):#Se llena el treeview de datos. 
        self.consulta = str(self.consulta)
        sql = ("select vacuna.nombre_vacuna, inventario.stock from inventario inner join vacuna on vacuna.id_vacuna = inventario.vacuna_id_vacuna where vacunatorio_id_vacunatorio = " + self.consulta)
        data = self.db.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                self.treeview.insert("", "end", text = i[0], values = (i[1]))
            self.data = data#Actualiza la data"""

    def llamar(self):
        insertar_inventario(self.db, self, self.consulta)

    def modif(self):
        sql = "select * from inventario where vacunatorio_id_vacunatorio =" + self.consulta
        row_data = self.db.run_select_filter(sql, None)
        modificar_inventario(self.db, self, row_data, self.consulta)

    def elim(self):

        a = self.treeview.focus()
        b = []
        for i in a:
            if i != "I":
                b.append(i)
        b = "".join(b)

        sql = "delete from inventario where vacunatorio_id_vacunatorio = " + self.consulta + " and vacuna_id_vacuna = " + b 
        self.db.run_sql(sql, None)
        self.llenar_treeview_inventario()

    def comprobar(self):#Se llena el treeview de datos. 
        self.consulta = str(self.consulta)
        sql = ("select * from inventario where vacunatorio_id_vacunatorio = " + self.consulta)

        data = self.db.run_select(sql)
        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                print(i[1])

class insertar_inventario:
    def __init__(self, db, padre, consulta):
        self.padre = padre
        self.db = db
        self.consulta = consulta
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):#Settings
        self.insert_datos.geometry('400x760')
        self.insert_datos.title("Insertar Inventario")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):#Labels
        tk.Label(self.insert_datos, text =  "ID Vacuna ").place(x = 10, y = 10, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "Stock: ").place(x = 10, y = 143, width = 130, height = 20)
    
    def config_entry(self):#Se configuran los inputs

        self.entry_stock = tk.Entry(self.insert_datos)
        self.entry_stock.place(x = 170, y = 143, width = 150, height = 30)
        sql2 = "select * from vacuna"
        self.data_vacuna = self.db.run_select(sql2)
        lista_vacuna = pd.DataFrame(self.data_vacuna)

        self.combo_vacuna = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_vacuna.place(x = 170, y = 10)
        self.combo_vacuna["values"] = lista_vacuna[1].tolist()
        self.combo_vacuna.bind("<<ComboboxSelected>>", self.prueba2)

    def prueba2(self, event):
        print(self.data_vacuna)
        print("b : ", self.combo_vacuna.get())   
        for i in self.data_vacuna:
            print(i)
            if self.combo_vacuna.get() in i:
                print(i[0])
                self.vacuna_fin = i[0]
            else:
                pass
    
    def config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar", 
            command = self.insertar).place(x=0, y =720, width = 200, height = 40)
        tk.Button(self.insert_datos, text = "Cancelar", 
            command = self.insert_datos.destroy).place(x=200, y =720, width = 200, height = 40)

    def insertar(self): #Insercion en la base de datos.
        sql = "SET FOREIGN_KEY_CHECKS=0;"
        self.db.run_sql(sql, None)
        self.padre.comprobar()

        sql = """insert into inventario(vacunatorio_id_vacunatorio, vacuna_id_vacuna, stock) 
        values (%(vacunatorio_id_vacunatorio)s, %(vacuna_id_vacuna)s, %(stock)s)"""
 
        self.db.run_sql(sql, {"vacunatorio_id_vacunatorio": self.consulta,
                            "vacuna_id_vacuna": self.vacuna_fin,
                                "stock": self.entry_stock.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_inventario()



class modificar_inventario:#Clase para modificar
    def __init__(self, db, padre, row_data, consulta):
        self.padre = padre
        self.db = db
        self.row_data =  row_data
        self.consulta = consulta
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):#Settings
        self.insert_datos.geometry('400x760')
        self.insert_datos.title("Modificar Inventario")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):#Labels
        tk.Label(self.insert_datos, text =  "ID Vacuna ").place(x = 10, y = 10, width = 130, height = 20)
        tk.Label(self.insert_datos, text =  "Stock: ").place(x = 10, y = 143, width = 130, height = 20)
    
    def config_entry(self):#Se configuran los inputs
        self.entry_stock = tk.Entry(self.insert_datos)
        self.entry_stock.place(x = 170, y = 143, width = 150, height = 30)

        self.entry_stock.insert(0, self.row_data[0][2])

        sql2 = "select * from vacuna"
        self.data_vacuna = self.db.run_select(sql2)
        lista_vacuna = pd.DataFrame(self.data_vacuna)

        self.combo_vacuna = ttk.Combobox(self.insert_datos, width=20, state = "readonly")
        self.combo_vacuna.place(x = 170, y = 10)
        self.combo_vacuna["values"] = lista_vacuna[1].tolist()
        self.combo_vacuna.bind("<<ComboboxSelected>>", self.prueba2)

    def prueba2(self, event):
        print(self.data_vacuna)
        print("b : ", self.combo_vacuna.get())   
        for i in self.data_vacuna:
            print(i)
            if self.combo_vacuna.get() in i:
                print(i[0])
                self.vacuna_fin = i[0]
            else:
                pass
        
    def config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar", command = self.modificar).place(x=0, y =720, width = 200, height = 40)
        tk.Button(self.insert_datos, text = "Cancelar", command = self.insert_datos.destroy).place(x=200, y=720, width=200, height=40)


    def modificar(self): #Insercion en la base de datos. 
        print(self.row_data)
        print(self.consulta)

        sql = "update inventario set stock = %(stock)s where vacunatorio_id_vacunatorio = " + self.consulta + " and vacuna_id_vacuna = %(vacuna_id_vacuna)s"""

        print(sql)

        self.db.run_sql(sql, {"vacunatorio_id_vacunatorio": self.consulta,
                            "vacuna_id_vacuna": self.vacuna_fin,
                              "stock": self.entry_stock.get()})

        self.insert_datos.destroy()
        self.padre.llenar_treeview_inventario()

