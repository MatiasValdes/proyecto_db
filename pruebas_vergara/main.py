import tkinter as tk
from tkinter import Menu
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from tkinter import PhotoImage
from tkinter import messagebox as MessageBox
from tkinter import ttk
from PIL import Image, ImageTk

from clases.database import Database
from clases.vacuna import vacuna
from clases.ciudad import ciudad
from clases.paciente import paciente
from clases.vacunatorio import vacunatorio
from clases.trabajador import trabajador
from clases.graficos import graficos
# clases

class App:
    def __init__(self, db):
        self.db = db

        self.root = tk.Tk()


        # size ventana y nombre
        self.root.geometry("765x400")
        self.root.title("Control Vacunatorio")
        self.root.configure(bg="#57A8D6")

        self.__menubar()
        self.__crea_botones_principales()
        self.__agrega_imagen_principal()

        #ejecutar gui
        self.root.mainloop()


    def __menubar(self):
        menubar = Menu(self.root) 
        self.root.config(menu=menubar)
        
        file_menu = Menu(menubar, tearoff=False)
        file_menu.add_command(label='Visualizar', command=self.__graficos)
        file_menu.add_separator()
        file_menu.add_command(label="Salir", command=self.__salir)

        visualizar_menu = Menu(menubar, tearoff=False)
        #visualizar_menu.add_command(label='Vacuna', command=self.__mostrar_vacuna)
        #visualizar_menu.add_command(label='Ciudad', command=self.__mostrar_ciudad)

        menubar.add_cascade(label="Opciones", menu=file_menu)
        # menubar.add_cascade(label="Visualizar", menu=visualizar_menu)

        help_menu = Menu(menubar, tearoff=False)
        help_menu.add_command(label="Ayuda")
        help_menu.add_separator()
        help_menu.add_command(label="Acerca de...", command=self.__mostrar_mensaje)

        menubar.add_cascade(label="Ayuda", menu=help_menu)

    def __mostrar_vacuna(self, button):
        vacuna(self.root, self.db)

    def __mostrar_paciente(self, button):
        paciente(self.root, self.db)

    def __mostrar_ciudad(self, button):
        ciudad(self.root, self.db)

    def __mostrar_vacunatorio(self, button):
        # print("vacunatorio")
        vacunatorio(self.root, self.db)

    def __mostrar_trabajador(self, button):
        # print("trabajador")
        trabajador(self.root, self.db)


    def __agrega_imagen_principal(self):
        frame = LabelFrame(self.root, text = "", relief = tk.FLAT)
        frame.place(x=150, y=20,relwidth=0.58, relheight=0.80)
        frame.configure(bg="#576CD6")

        image = Image.open("imagen.png")
        photo = ImageTk.PhotoImage(image.resize((450, 320), Image.ANTIALIAS))
        label = Label (frame, image=photo)
        label.image = photo
        label.pack()


    def __crea_botones_principales(self):
        padx = 2
        pady = 2

        frame = LabelFrame(self.root, text="", relief=tk.GROOVE)
        frame.place(x=10,y=15,width=120, relheight=0.95)
        frame.configure(bg="#576CD6")

        boton1 = Button(frame, text="Vacuna", width = 10,  height = 3,foreground="#52F012", background="#000000")
        boton1.grid(row=0,column=0, padx=padx, pady=pady)
        boton1.bind("<Button-1>", self.__mostrar_vacuna)

        boton2 = Button(frame, text="Paciente", width = 10,  height = 3,foreground="#52F012", background="#000000")
        boton2.grid(row=20,column=0, padx=padx, pady=pady)
        boton2.bind("<Button-1>", self.__mostrar_paciente)

        boton3 = Button(frame, text="Ciudad", width = 10,  height = 3,foreground="#52F012", background="#000000")
        boton3.grid(row=40,column=0, padx=padx, pady=pady)
        boton3.bind("<Button-1>", self.__mostrar_ciudad)


        frame2 = LabelFrame(self.root, text="", relief=tk.GROOVE)
        frame2.place(x=630, y=15, width=120, relheight = 0.95)
        frame2.configure(bg="#576CD6")

        boton4 = Button(frame2, text="Trabajador", width = 10, height = 3, foreground="#52F012", background="#000000")
        boton4.grid(row=0,column=0, padx=padx, pady=pady)
        boton4.bind("<Button-1>", self.__mostrar_trabajador)

        boton5 = Button(frame2, text="Vacunatorio", width = 10,  height = 3, foreground="#52F012", background="#000000")
        boton5.grid(row=20,column=0, padx=padx, pady=pady)
        boton5.bind("<Button-1>", self.__mostrar_vacunatorio)

        boton6 = Button(frame2, text="Salir", width = 10,  height = 3,foreground="#52F012", background="#000000")
        boton6.grid(row=40,column=0, padx=padx, pady=pady)
        boton6.bind("<Button-1>", self.__salir2)

        #img_salir = PhotoImage(file="cerrar.png")
        #boton_salir = Button(frame2, image=img_salir, width=200).place(x=100, y=100)
        #boton_salir.place(x=610,y=300)
        #boton_salir.grid(row=0, column=0, padx=padx, pady=pady)
        #boton_salir.bind(self.root.destroy)
    
    
    def __mostrar_mensaje(self):
        fichero = open("mensaje.txt", "r")
        read_fichero = fichero.readlines()
        fichero.close()
        mensaje = []
        for i in range(len(read_fichero)):
            mensaje.append(read_fichero[i])

        mensaje_final = "".join(mensaje)
        MessageBox.showinfo("Informacion", mensaje_final)

    def __salir(self):
        resultado = MessageBox.askquestion("Salir",
                "¿Está seguro que desea salir?")
        if resultado == "yes":
            self.root.destroy()
        else:
            pass

    def __salir2(self, button):
        resultado = MessageBox.askquestion("Salir",
                "¿Está seguro que desea salir?")
        if resultado == "yes":
            self.root.destroy()
        else:
            pass
    
    def __graficos(self):
        graficos(self.root, self.db)

def main():
    db = Database()
    App(db)

if __name__ == "__main__":
    main()
